package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FinalLossAssetDTO {
    private final String assetName;
    private final double loss;
}
