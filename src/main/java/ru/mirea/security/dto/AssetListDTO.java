package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@Builder
public class AssetListDTO {
    private final String name;
    private final boolean isActive;
}
