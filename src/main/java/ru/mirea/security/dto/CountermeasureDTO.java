package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CountermeasureDTO {
    private final String countermeasureName;
    private final double diff;
}
