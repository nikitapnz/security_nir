package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@Builder
public class DangerDTO {
    @NotBlank
    private final String name;
    private final double chance;
}
