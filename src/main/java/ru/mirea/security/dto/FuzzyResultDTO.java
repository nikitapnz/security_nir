package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FuzzyResultDTO {
    private final String vulnerabilityName;
    private final String dangerName;
    private final double risk;

    private final Double low;
    private final Double high;
    private final Double middle;
}
