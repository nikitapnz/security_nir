package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ChoiceDTO {
    private final String name;
    private final double point;
    private final boolean selected;
}
