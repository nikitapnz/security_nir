package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.mirea.security.entity.Asset;
import ru.mirea.security.entity.AssetDanger;
import ru.mirea.security.entity.Countermeasure;
import ru.mirea.security.entity.Danger;

@Getter
@Builder
public class LossDTO {
    private final Asset asset;
    private final String vulnerabilityName;
    private final double vulnerabilityPoint;
    private final AssetDanger assetDanger;
    private final Countermeasure countermeasure;
}
