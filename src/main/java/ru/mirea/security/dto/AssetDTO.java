package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.mirea.security.entity.Question;

import java.util.List;

@Getter
@Builder
public class AssetDTO {
    private final String name;
    private final int price;
    private final boolean active;
    private final List<QuestionDTO> questions;
    private final List<DangerDTO> dangers;
}
