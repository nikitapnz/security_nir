package ru.mirea.security.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class QuestionDTO {
    private final String name;
    private final List<ChoiceDTO> choices;
}
