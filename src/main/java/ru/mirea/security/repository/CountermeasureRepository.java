package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.Countermeasure;

@Repository
public interface CountermeasureRepository extends JpaRepository<Countermeasure, String> {
}
