package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, String> {
}
