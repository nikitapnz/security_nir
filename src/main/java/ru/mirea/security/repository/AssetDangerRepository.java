package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.AssetDanger;
import ru.mirea.security.entity.Danger;

import java.util.List;

@Repository
public interface AssetDangerRepository extends JpaRepository<AssetDanger, Integer> {

    @Query("" +
            "select distinct ad " +
            "from AssetDanger ad " +
            "inner join ad.asset a " +
            "where a.name = :assetName")
    List<AssetDanger> findAllByActiveAssets(String assetName);

    List<AssetDanger> findAllByDangerNameInAndAssetName(List<String> names, String assetName);
}
