
package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.Asset;
import ru.mirea.security.entity.Question;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, String> {
    @Query("" +
            "select distinct q " +
            "from Question q " +
            "join fetch q.choices c " +
            "inner join q.countermeasure countm " +
            "inner join countm.dangers d " +
            "inner join d.assetDangers a " +
            "inner join a.asset ass " +
            "where ass = :asset ")
    List<Question> getQuestions(Asset asset);

}
