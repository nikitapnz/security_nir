package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.mirea.security.dto.VulnerabilityDTO;
import ru.mirea.security.entity.Asset;

import java.util.List;

@Repository
public interface AssetRepository extends JpaRepository<Asset, String> {
    @Query("" +
            "select new ru.mirea.security.dto.VulnerabilityDTO(a, q.vulnerability, countm, sum(ch.point)) " +
            "from Asset a " +
            "inner join a.answers ans " +
            "inner join ans.choice ch " +
            "inner join ch.question q " +
            "inner join q.countermeasure countm " +
            "where a.name = :assetName and a.isActive = true " +
            "group by a.name, q.vulnerability " +
            "having sum(ch.point) >= 0.7 ")
    List<VulnerabilityDTO> getVulnerability(String assetName);
}
