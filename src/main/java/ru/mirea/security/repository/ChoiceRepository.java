package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.Choice;

import java.util.Optional;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, Integer> {
    Choice findByQuestionNameAndName(String questionName, String choiceName);
}
