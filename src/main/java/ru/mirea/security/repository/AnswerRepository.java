package ru.mirea.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mirea.security.entity.Answer;
import ru.mirea.security.entity.Asset;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, String> {
    List<Answer> findByAsset(Asset asset);
    void deleteAllByAsset(Asset asset);
}
