package ru.mirea.security.domain;

import lombok.Builder;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
@Builder
public class DangerLevel {
    private final Type type;
    private final double value;

    @Getter
    public enum Type {
        LOW(0),
        MIDDLE(1),
        HIGH(2);

        private final int sortOrder;

        Type(int sortOrder) {
            this.sortOrder = sortOrder;
        }

        public static Type equalsTypes(List<DangerLevel> typeList) {
            int result = typeList.stream().mapToInt(e -> e.getType().getSortOrder()).sum();
            double doubleRes = Math.round((double) result / (double) typeList.size());
            int type = (int) Math.round(doubleRes);
            return Arrays.stream(values()).filter(e -> e.getSortOrder() == type)
                    .findFirst()
                    .orElse(null);
        }
    }
}
