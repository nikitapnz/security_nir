package ru.mirea.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Danger {

    @Id
    private String name;

    @OneToMany(mappedBy = "danger")
    private List<AssetDanger> assetDangers;

    @JsonIgnore
    @ManyToMany(mappedBy = "dangers")
    private List<Countermeasure> countermeasure;
}
