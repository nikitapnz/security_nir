package ru.mirea.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Countermeasure {
    @Id
    private String name;

    private Double efficiency;

    private Integer price;

    private String source;

    private String comment;

    @JsonIgnore
    @OneToMany(mappedBy = "countermeasure")
    private List<Question> questions;

    @ManyToMany
    @JoinTable(
            name = "danger_countermeasure",
            joinColumns = { @JoinColumn(name = "countermeasure_name") },
            inverseJoinColumns = { @JoinColumn(name = "danger_name") }
    )
    private List<Danger> dangers;
}
