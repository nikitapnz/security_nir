package ru.mirea.security.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Test {
    @Id
    private String name;

    public Test(String name) {
        this.name = name;
    }
}
