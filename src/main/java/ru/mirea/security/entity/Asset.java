package ru.mirea.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Asset {
    @Id
    private String name;

    private Integer price;

    private Boolean isActive;

    @OneToMany(mappedBy = "asset", fetch = FetchType.LAZY)
    private List<Answer> answers;

    @OneToMany(mappedBy = "asset", fetch = FetchType.LAZY)
    private List<AssetDanger> assetDangers;
}
