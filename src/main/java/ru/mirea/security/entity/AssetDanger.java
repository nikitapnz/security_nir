package ru.mirea.security.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter @Setter
public class AssetDanger {
    @Id
    private Integer id;

    private Double chance;

    @ManyToOne
    @JoinColumn(name = "asset_name")
    private Asset asset;

    @ManyToOne
    @JoinColumn(name = "danger_name")
    private Danger danger;
}
