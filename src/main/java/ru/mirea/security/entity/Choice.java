package ru.mirea.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Choice {
    @Id
    private Integer id;

    private String name;

    private Double point;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_name")
    private Question question;

    @OneToMany(mappedBy = "choice", fetch = FetchType.LAZY)
    private List<Answer> answers;
}
