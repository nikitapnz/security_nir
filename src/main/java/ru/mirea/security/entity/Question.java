package ru.mirea.security.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter
public class Question {

    @Id
    private String name;

    private String vulnerability;

    private Integer block;

    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "question")
    private List<Choice> choices;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countermeasure_name")
    private Countermeasure countermeasure;

}
