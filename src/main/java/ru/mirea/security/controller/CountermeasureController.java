package ru.mirea.security.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.security.core.parameters.P;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mirea.security.dto.CountermeasureDTO;
import ru.mirea.security.dto.FinalLossAssetDTO;
import ru.mirea.security.dto.LossDTO;
import ru.mirea.security.entity.Asset;
import ru.mirea.security.entity.Countermeasure;
import ru.mirea.security.service.LossService;

import javax.validation.constraints.NotBlank;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/countermeasures")
@RequiredArgsConstructor
public class CountermeasureController {
    private final LossService lossService;

    @PostMapping("/roi")
    public List<CountermeasureDTO> getCountermeasureRIO(@Validated @RequestBody Name name) {
        Map<Countermeasure, List<LossDTO>> countermeasureMap =  StreamEx.of(lossService.getLosses(name.getName()))
                .groupingBy(LossDTO::getCountermeasure);

        return EntryStream.of(countermeasureMap)
                .map(lossDTOS -> CountermeasureDTO.builder()
                        .countermeasureName(lossDTOS.getKey().getName())
                        .diff(lossDTOS.getValue()
                                .stream()
                                .mapToDouble(a -> a.getAssetDanger().getChance() * a.getVulnerabilityPoint() * (double) a.getAsset().getPrice()).sum() - lossDTOS.getKey().getPrice() * 5)
                        .build())
                .filter(e -> e.getDiff() > 0 )
                .sorted(Comparator.comparing(CountermeasureDTO::getDiff).reversed())
                .limit(5)
                .toList();
    }

    @PostMapping("/loss")
    public FinalLossAssetDTO getLossByAsset(@Validated @RequestBody Name name) {
        List<LossDTO> losses = lossService.getLosses(name.getName());

        return FinalLossAssetDTO.builder()
                    .assetName(name.getName())
                    .loss(losses.stream().mapToDouble(a -> a.getAssetDanger().getChance() * a.getVulnerabilityPoint() * (double) a.getAsset().getPrice()).max().orElse(0))
                    .build();
    }

    @Data
    public static class Name {
        @NotBlank
        private String name;
    }
}
