package ru.mirea.security.controller;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mirea.security.dto.AssetDTO;
import ru.mirea.security.dto.AssetListDTO;
import ru.mirea.security.service.AssetService;

import javax.validation.constraints.NotBlank;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/assets")
@RequiredArgsConstructor
public class AssetController {
    private final AssetService assetService;

    @GetMapping
    public List<AssetListDTO> getAssets() {
        return assetService.getAssets().stream()
                .sorted(Comparator.comparing(AssetListDTO::isActive).reversed())
                .collect(Collectors.toList());
    }

    @PostMapping("/asset")
    public AssetDTO getAsset(@Validated @RequestBody Name name) {
        return assetService.getAsset(name.getName());
    }

    @PutMapping
    public AssetDTO update(@Validated @RequestBody AssetDTO assetDTO) {
        return assetService.save(assetDTO);
    }

    @Data
    public static class Name {
        @NotBlank
        private String name;
    }
}
