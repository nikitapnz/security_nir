package ru.mirea.security.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mirea.security.domain.DangerLevel;
import ru.mirea.security.dto.FuzzyResultDTO;
import ru.mirea.security.service.fuzzy.FuzzyLogicService;

import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/fuzzy")
@RequiredArgsConstructor
public class FuzzyLogicController {
    private final FuzzyLogicService fuzzyLogicService;

    @PostMapping("/risk")
    public List<FuzzyResultDTO> getRisks(@Validated @RequestBody Name name) {
        return fuzzyLogicService.getRisk(name.getName()).stream()
                .sorted(Comparator.comparing(FuzzyResultDTO::getRisk).reversed())
                .collect(Collectors.toList());
    }

    @Data
    public static class Name {
        @NotBlank
        private String name;
    }
}
