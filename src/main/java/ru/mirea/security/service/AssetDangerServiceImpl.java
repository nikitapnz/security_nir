package ru.mirea.security.service;

import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;
import ru.mirea.security.dto.DangerDTO;
import ru.mirea.security.entity.AssetDanger;
import ru.mirea.security.entity.Danger;
import ru.mirea.security.repository.AssetDangerRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AssetDangerServiceImpl implements AssetDangerService {
    private final AssetDangerRepository assetDangerRepository;

    @Override
    @Transactional
    public List<DangerDTO> save(String assetName, List<DangerDTO> dangerDTO) {
        Map<String, AssetDanger> existingDangers = assetDangerRepository.findAllByDangerNameInAndAssetName(
                StreamEx.of(dangerDTO).map(DangerDTO::getName).toList(),
                assetName
        ).stream().collect(Collectors.toMap(e -> e.getDanger().getName(), Function.identity()));

        dangerDTO.forEach(dto -> {
            AssetDanger assetDanger = existingDangers.get(dto.getName());
            if (assetDanger == null) { throw new IllegalStateException(); }

            assetDanger.setChance(dto.getChance());
        });

        assetDangerRepository.saveAll(existingDangers.values());
        return dangerDTO;
    }

    @Override
    public List<DangerDTO> getDangers(String assetName) {
        return assetDangerRepository.findAllByActiveAssets(assetName).stream()
                .map(e -> DangerDTO.builder().name(e.getDanger().getName()).chance(e.getChance()).build())
                .collect(Collectors.toList());
    }
}
