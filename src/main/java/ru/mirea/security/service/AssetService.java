package ru.mirea.security.service;

import ru.mirea.security.dto.AssetDTO;
import ru.mirea.security.dto.AssetListDTO;
import ru.mirea.security.dto.VulnerabilityDTO;

import java.util.List;

public interface AssetService {
    List<AssetListDTO> getAssets();
    AssetDTO getAsset(String name);

    AssetDTO save(AssetDTO assetDTO);

    List<VulnerabilityDTO> getVulnerability(String assetName);
}
