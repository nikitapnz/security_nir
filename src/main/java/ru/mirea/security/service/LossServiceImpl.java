package ru.mirea.security.service;

import lombok.RequiredArgsConstructor;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.mirea.security.dto.LossDTO;
import ru.mirea.security.dto.VulnerabilityDTO;
import ru.mirea.security.entity.Asset;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class LossServiceImpl implements LossService {
    private final AssetService assetService;

    @Override
    @Transactional
    public List<LossDTO> getLosses(String assetName) {
        List<VulnerabilityDTO> vulnerabilityDTOS = assetService.getVulnerability(assetName);
        if (CollectionUtils.isEmpty(vulnerabilityDTOS)) { return Collections.emptyList(); }
        Map<Asset, List<VulnerabilityDTO>> groupedVulnerabilityByAsset = StreamEx.of(vulnerabilityDTOS)
                .groupingBy(VulnerabilityDTO::getAsset);

        return EntryStream.of(groupedVulnerabilityByAsset)
                .flatMap(entry -> StreamEx.of(entry.getValue())
                        .flatMap(vulnerabilityDTO -> vulnerabilityDTO.getAsset().getAssetDangers()
                                .stream().map(assetDanger ->
                                        LossDTO.builder()
                                                .asset(vulnerabilityDTO.getAsset())
                                                .assetDanger(assetDanger)
                                                .vulnerabilityPoint(vulnerabilityDTO.getPoint())
                                                .countermeasure(vulnerabilityDTO.getCountermeasure())
                                                .vulnerabilityName(vulnerabilityDTO.getVulnerability())
                                                .build())))
                .toList();
    }
}
