package ru.mirea.security.service.fuzzy;

import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;
import ru.mirea.security.domain.DangerLevel;
import ru.mirea.security.dto.FuzzyResultDTO;
import ru.mirea.security.dto.LossDTO;
import ru.mirea.security.service.LossService;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class FuzzyLogicServiceImpl implements FuzzyLogicService {
    private final LossService lossService;

    @Override
    @Transactional
    public List<FuzzyResultDTO> getRisk(String assetName) {
        List<LossDTO> losses = lossService.getLosses(assetName);
        double maxLoss = losses.stream()
                .mapToDouble(a -> a.getAssetDanger().getChance() * a.getVulnerabilityPoint() * (double) a.getAsset().getPrice())
                .max()
                .orElse(0);

        return StreamEx.of(losses)
                .map(loss -> {
                    double currentLoss = loss.getAssetDanger().getChance() * loss.getVulnerabilityPoint() * (double) loss.getAsset().getPrice();
                    List<DangerLevel> aggregationResult = aggregation(loss.getAssetDanger().getChance(), loss.getVulnerabilityPoint(), currentLoss, maxLoss);
                    Map<DangerLevel.Type, Double> accumulationResult = accumulation(aggregationResult);
                    double deffazufiction = integral(0, 1, accumulationResult, true) / integral(0, 1, accumulationResult, false);

                    return FuzzyResultDTO.builder()
                        .risk(deffazufiction)
                        .dangerName(loss.getAssetDanger().getDanger().getName())
                        .vulnerabilityName(loss.getVulnerabilityName())
                        .high(accumulationResult.get(DangerLevel.Type.HIGH))
                        .middle(accumulationResult.get(DangerLevel.Type.MIDDLE))
                        .low(accumulationResult.get(DangerLevel.Type.LOW))
                        .build();
                }).toList();
    }

    private List<DangerLevel> aggregation(double dangerChance, double vulnerabilityResult, double loss, double maxLoss) {
        List<DangerLevel> dangerLevels = membershipFunction(dangerChance, 1);
        List<DangerLevel> vulnerabilityLevels = membershipFunction(vulnerabilityResult, 1);
        List<DangerLevel> lossValues = membershipFunction(loss, maxLoss);
        List<DangerLevel> result = new ArrayList<>();

        dangerLevels.forEach(dangerLevel ->
                vulnerabilityLevels.forEach(vulnerabilityLevel ->
                        lossValues.forEach(lossValue -> {
                            List<DangerLevel> equalsDangerLevels = Arrays.asList(dangerLevel, vulnerabilityLevel, lossValue);
                            DangerLevel.Type finalType = DangerLevel.Type.equalsTypes(equalsDangerLevels);
                            double minValue = StreamEx.of(equalsDangerLevels)
                                    .filter(Objects::nonNull)
                                    .map(DangerLevel::getValue)
                                    .min(Comparator.comparing(e -> e))
                                    .orElse(0d);

                            result.add(DangerLevel.builder().type(finalType).value(minValue).build());
                        })));

        return result;
    }

    private List<DangerLevel> membershipFunction(double value, double maxOx) {
        return Arrays.asList(
                DangerLevel.builder().type(DangerLevel.Type.LOW).value(getLowValue(value, maxOx)).build(),
                DangerLevel.builder().type(DangerLevel.Type.MIDDLE).value(getMiddleLevel(value, maxOx)).build(),
                DangerLevel.builder().type(DangerLevel.Type.HIGH).value(getHighValue(value, maxOx)).build()
        );
    }

    private double getHighValue(double value, double maxOx) {
        if ((value >= (maxOx / 5d) * 3) && (value < (maxOx / 5d) * 4)) {
            return calcRename((maxOx / 5d) * 3, 0d, (maxOx / 5d) * 4, 1d, value);
        }

        if (value >= (maxOx / 5d) * 4) {
            return 1;
        }

        return 0;
    }

    private double getMiddleLevel(double value, double maxOx) {
        if ((value >= maxOx / 5d) && (value < (maxOx / 5d) * 2)) {
            return calcRename(maxOx / 5d, 0d, (maxOx / 5d) * 2, 1d, value);
        }

        if ((value >= (maxOx / 5d) * 2) && (value <= (maxOx / 5d) * 3)) {
            return 1;
        }

        if ((value > (maxOx / 5d) * 3) && (value <= (maxOx / 5d) * 4)) {
            return calcRename((maxOx / 5d) * 3, 1d, (maxOx / 5d) * 4, 0d, value);
        }

        return 0;
    }

    private double getLowValue(double value, double maxOx) {
        if (value <= maxOx / 5d) {
            return 1;
        }

        if ((value > maxOx / 5d) && (value <= (maxOx / 5d) * 2)) {
            return calcRename(maxOx / 5d, 1d, (maxOx / 5d) * 2, 0d, value);
        }

        return 0;
    }

    private double calcRename(double x1, double y1, double x2, double y2, double x) {
        return -1 * (((x1* y2 - x2 * y1) + (y1 - y2) * x) / (x2 - x1));
    }

    private Map<DangerLevel.Type, Double> accumulation(List<DangerLevel> result) {
        return StreamEx.of(result)
                .sortedBy(DangerLevel::getType)
                .groupRuns((f, s) -> f.getType().equals(s.getType()))
                .map(list -> list.stream().max(Comparator.comparing(DangerLevel::getValue)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(e -> e.getValue() > 0)
                .mapToEntry(DangerLevel::getType, DangerLevel::getValue)
                .toMap();
    }

    private double u(double x, Map<DangerLevel.Type, Double> finalMap) {
        return StreamEx.of(membershipFunction(x, 1))
                .filter(e -> finalMap.get(e.getType()) != null)
                .map(e -> {
                    if (e.getValue() > finalMap.get(e.getType())) {
                        return DangerLevel.builder()
                                .value(finalMap.get(e.getType()))
                                .type(e.getType())
                                .build();
                    }
                    return e;
                })
                .max(Comparator.comparing(DangerLevel::getValue))
                .map(DangerLevel::getValue)
                .orElse(0d);
    }

    private double integral(double a, double b, Map<DangerLevel.Type, Double> acumlationResult, boolean flag) {
        int n = 1000;

        double h = ((b - a) / n);
        double area = 0.0;
        for (int i = 0; (i < n); i++) {
            double _f = u((a + (i * h)), acumlationResult);

            if (flag) {
                _f = _f * (a + (i * h));
            }

            double _multiply = (h * _f);
            double _plus = (area + _multiply);
            area = _plus;
        }
        return area;
    }
}
