package ru.mirea.security.service.fuzzy;

import ru.mirea.security.domain.DangerLevel;
import ru.mirea.security.dto.FuzzyResultDTO;

import javax.transaction.Transactional;
import java.util.List;

public interface FuzzyLogicService {

    List<FuzzyResultDTO> getRisk(String assetName);
}
