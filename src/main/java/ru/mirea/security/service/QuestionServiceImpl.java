package ru.mirea.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.mirea.security.dto.AssetDTO;
import ru.mirea.security.dto.ChoiceDTO;
import ru.mirea.security.dto.QuestionDTO;
import ru.mirea.security.entity.Answer;
import ru.mirea.security.entity.Asset;
import ru.mirea.security.entity.Choice;
import ru.mirea.security.entity.Question;
import ru.mirea.security.repository.AnswerRepository;
import ru.mirea.security.repository.ChoiceRepository;
import ru.mirea.security.repository.QuestionRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final ChoiceRepository choiceRepository;

    @Override
    public List<QuestionDTO> getQuestions(Asset asset) {
        List<Question> questions = questionRepository.getQuestions(asset);
        if (CollectionUtils.isEmpty(questions)) { throw new IllegalStateException("Questions not found"); }
        List<Answer> answers = answerRepository.findByAsset(asset);
        return questions.stream().map(e -> {
            List<ChoiceDTO> choiceDTOS = e.getChoices().stream().map(ch -> {
                Answer answer = answers.stream()
                        .filter(a -> Objects.equals(a.getChoice().getName(), ch.getName())
                                && Objects.equals(a.getChoice().getQuestion().getName(), e.getName()))
                        .findFirst()
                        .orElse(null);
                return ChoiceDTO.builder()
                            .name(ch.getName())
                            .point(ch.getPoint())
                            .selected(answer != null)
                        .build();
            }).collect(Collectors.toList());

            return QuestionDTO.builder()
                        .choices(choiceDTOS)
                        .name(e.getName())
                    .build();
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void saveAnswers(Asset asset, List<QuestionDTO> questionDTOS) {
        answerRepository.deleteAllByAsset(asset);

        List<Answer> answers = questionDTOS.stream()
                .map(e -> e.getChoices().stream().filter(ChoiceDTO::isSelected).map(choiceDTO -> {
                        Answer a = new Answer();
                        Choice choice = choiceRepository.findByQuestionNameAndName(e.getName(), choiceDTO.getName());
                        a.setAsset(asset);
                        a.setChoice(choice);
                        return a;
        }).findFirst().orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(answers)) {
            answerRepository.saveAll(answers);
        }
    }
}
