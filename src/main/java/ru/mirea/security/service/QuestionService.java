package ru.mirea.security.service;

import ru.mirea.security.dto.AssetDTO;
import ru.mirea.security.dto.QuestionDTO;
import ru.mirea.security.entity.Asset;

import java.util.List;

public interface QuestionService {
    List<QuestionDTO> getQuestions(Asset asset);

    void saveAnswers(Asset asset, List<QuestionDTO> questionDTOS);
}
