package ru.mirea.security.service;

import ru.mirea.security.dto.DangerDTO;

import java.util.List;

public interface AssetDangerService {
    List<DangerDTO> save(String assetName, List<DangerDTO> dangerDTO);
    List<DangerDTO> getDangers(String assetName);
}
