package ru.mirea.security.service;

import ru.mirea.security.dto.LossDTO;
import ru.mirea.security.entity.Asset;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

public interface LossService {
    List<LossDTO> getLosses(String assetName);
}
