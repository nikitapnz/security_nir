package ru.mirea.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mirea.security.dto.*;
import ru.mirea.security.entity.Asset;
import ru.mirea.security.repository.AssetRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AssetServiceImpl implements AssetService {
    private final AssetRepository assetRepository;
    private final QuestionService questionService;
    private final AssetDangerService assetDangerService;

    @Override
    public List<AssetListDTO> getAssets() {
        return assetRepository.findAll().stream().map(e ->
                AssetListDTO.builder()
                    .isActive(e.getIsActive())
                    .name(e.getName())
                    .build()
        ).collect(Collectors.toList());
    }

    @Override
    public AssetDTO getAsset(String name) {
        Asset asset = assetRepository.findById(name).orElse(null);
        if (asset == null) {
            throw new IllegalStateException();
        }
        List<QuestionDTO> questionDTOS = questionService.getQuestions(asset);
        List<DangerDTO> dangerDTOS = assetDangerService.getDangers(name);

        return AssetDTO.builder()
                .price(asset.getPrice())
                .active(asset.getIsActive())
                .name(asset.getName())
                .questions(questionDTOS)
                .dangers(dangerDTOS)
                .build();
    }

    @Override
    @Transactional
    public AssetDTO save(AssetDTO assetDTO) {
        Asset asset = assetRepository.findById(assetDTO.getName()).orElse(null);
        if (asset == null) {
            throw new IllegalStateException();
        }
        asset.setIsActive(assetDTO.isActive());
        asset.setPrice(assetDTO.getPrice());
        assetRepository.save(asset);
        questionService.saveAnswers(asset, assetDTO.getQuestions());
        assetDangerService.save(assetDTO.getName(), assetDTO.getDangers());

        return assetDTO;
    }

    @Override
    public List<VulnerabilityDTO> getVulnerability(String assetName) {

        return assetRepository.getVulnerability(assetName);
    }
}
