create table asset (
    name varchar(255) not null constraint asset_pkey primary key,
    is_active boolean default false,
    price int default 0
);

insert into asset (name) values('Денежные средства');
insert into asset (name) values('Недвижимость');
insert into asset (name) values('Акции и облигации');
insert into asset (name) values('Прикладное программное обеспечение');
insert into asset (name) values('Системное программное обеспечение');
insert into asset (name) values('Компьютерное оборудование');
insert into asset (name) values('Коммуникационное оборудование');
insert into asset (name) values('Носители информации (например, CD, DVD)');
insert into asset (name) values('Техническое оборудование (например, источники питания, кондиционеры, системы пожаротушения)');
insert into asset (name) values('Базы данных');
insert into asset (name) values('Документация');
insert into asset (name) values('Производственные запасы (сырье, готовая продукция)');
insert into asset (name) values('Коммерческая тайна');
insert into asset (name) values('Прочие нематериальные активы (лицензии)');
