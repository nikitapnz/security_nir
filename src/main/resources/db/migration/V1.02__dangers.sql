create table danger (
    name varchar(1024) not null constraint danger_pkey primary key
);

create table asset_danger (
    id int IDENTITY not null constraint asset_danger_pkey primary key,
    chance double not null default 0,
    asset_name varchar(255) not null
        constraint fkda7b49kv115uq8ogetm2vjrdw
            references asset,
    danger_name varchar(1024) not null
        constraint fk8rq7q9imy44qt0jl56n1kfgpc
            references danger
);

create table danger_countermeasure (
    danger_name varchar(1024) not null
        constraint fkda7b59kv115uq8ogetm2vjrdw
            references danger,
    countermeasure_name varchar(255) not null
        constraint fk8rq7q9imy44qt0j5456n1kfgpc
            references countermeasure
);

insert into danger (name) values('Кибератаки со стороны 3-их лиц (кража)');
insert into danger (name) values('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)');
insert into danger (name) values('Кибератака со стороны 3-их лиц (отказ в обслуживание)');
insert into danger (name) values('Программные сбои и отказы');
insert into danger (name) values('Аппаратные сбои и отказы');
insert into danger (name) values('Переманивание сотрудников организации конкурентами или партнерами');
insert into danger (name) values('Разглашение информации сотрудником отдела (преднамеренное)');
insert into danger (name) values('Разглашение информации сотрудником отдела (случайное)');
insert into danger (name) values('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации');
insert into danger (name) values('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации');
insert into danger (name) values('Порча имиджа или репутации сотрудником');
insert into danger (name) values('Порча имиджа или репутации 3-им лицом');
insert into danger (name) values('Ошибки проектирования и разработки технических средств');
insert into danger (name) values('Кража активов сотрудником организации');
insert into danger (name) values('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)');
insert into danger (name) values('Утечка конфиденциальной информации с переносных носителей информации (USB-флешки, диски и т.п.)');
insert into danger (name) values('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим вне контролируемой зоны, осуществляемое внешними нарушителями путем анализа трафика, проходящего по канал связи (сюда же можно отнести считывание информации с оптических каналов связи интернет провайдеров)');
insert into danger (name) values('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим внутри контролируемой зоны, осуществляемое внутренними нарушителями путем анализа трафика, проходящего по каналам связи (сюда же можно отнести считывание информации с оптических каналов связи, проложенных внутри здания)');
insert into danger (name) values('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)');
insert into danger (name) values('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)');
insert into danger (name) values('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)');
insert into danger (name) values('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)');
insert into danger (name) values('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)');
insert into danger (name) values('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)');
insert into danger (name) values('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)');
insert into danger (name) values('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)');
insert into danger (name) values('Перебои с электроэнергией');
insert into danger (name) values('Отказ системы кондиционирования в серверных');
insert into danger (name) values('Природные катастрофы (затопление, пожар, ураган, землетрясение и т.п.)');
insert into danger (name) values('Ложное срабатывание системы пожаротушения (и как результат порча собственности)');
