insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Системы обнаружения вторжений');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Использование DLP систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Использование firewall');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Повышение компьютерной грамотности персонала');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Системы обнаружения вторжений');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Использование DLP систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Использование firewall');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Повышение компьютерной грамотности персонала');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Анти-DDoS');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Системы обнаружения вторжений');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Использование DLP систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Использование firewall');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Повышение компьютерной грамотности персонала');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Программные сбои и отказы', 'Повышение компьютерной грамотности персонала');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Программные сбои и отказы', 'Контроль за неосторожными действиями пользователей');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Программные сбои и отказы', 'Использование антивирусных программ');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Аппаратные сбои и отказы', 'Использование антивирусных программ');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Аппаратные сбои и отказы', 'Использование более совершенного и отказоустойчивого оборудования');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (случайное)', 'Обеспечение защиты информации с помощью шифрования данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (случайное)', 'Повышение компьютерной грамотности персонала');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (случайное)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (преднамеренное)', 'Обеспечение защиты информации с помощью шифрования данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (преднамеренное)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Пропускная система на базе турникетов и контроллеров (СКУД)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Использование охранной сигнализации');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Физическая охрана (службы безопасности или охранники)');


insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации', 'Пропускная система на базе турникетов и контроллеров (СКУД)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации', 'Использование охранной сигнализации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Физическая охрана (службы безопасности или охранники)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Использование охранной сигнализации');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)', 'Использование DLP систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)', 'Использование firewall');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)', 'Обеспечение защиты информации с помощью шифрования данных');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с переносных носителей информации (USB-флешки, диски и т.п.)', 'Использование DLP систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с переносных носителей информации (USB-флешки, диски и т.п.)', 'Обеспечение защиты информации с помощью шифрования данных');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим вне контролируемой зоны, осуществляемое внешними нарушителями путем анализа трафика, проходящего по канал связи (сюда же можно отнести считывание информации с оптических каналов связи интернет провайдеров)', 'Обеспечение защиты информации с помощью шифрования данных');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим внутри контролируемой зоны, осуществляемое внутренними нарушителями путем анализа трафика, проходящего по каналам связи (сюда же можно отнести считывание информации с оптических каналов связи, проложенных внутри здания)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим внутри контролируемой зоны, осуществляемое внутренними нарушителями путем анализа трафика, проходящего по каналам связи (сюда же можно отнести считывание информации с оптических каналов связи, проложенных внутри здания)', 'Обеспечение защиты информации с помощью шифрования данных');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'Использование антивирусных программ');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Использование антивирусных программ');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Системы фильтрации электронной почты корпоративных почтовых ящиков');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)', 'Проведение тренингов и бесед по основным приемам социальной инженерии');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)', 'Обеспечение защиты информации с помощью шифрования данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)', 'Использование особых процедур подтверждения для всех, кто запрашивает и имеет доступ к конфиденциальной информации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Повышение компьютерной грамотности персонала');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Бэкап серверов');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Резервное копирование данных на облаке');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Повышение компьютерной грамотности персонала');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Физическая охрана (службы безопасности или охранники)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Использование охранной сигнализации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Физическая охрана (службы безопасности или охранники)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Использование охранной сигнализации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Физическая охрана (службы безопасности или охранники)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Видеонаблюдение');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Пропускная система на базе турникетов и контроллеров (СКУД)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Использование охранной сигнализации');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Перебои с электроэнергией', 'Использование бесперебойного электроснабжения / резервного питания');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Отказ системы кондиционирования в серверных', 'Использование резервных кондиционеров');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Отказ системы кондиционирования в серверных', 'Использование бесперебойного электроснабжения / резервного питания');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Природные катастрофы (затопление, пожар, ураган, землетрясение и т.п.)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Природные катастрофы (затопление, пожар, ураган, землетрясение и т.п.)', 'Использование бесперебойного электроснабжения / резервного питания');

-- insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ложное срабатывание системы пожаротушения (и как результат порча собственности)', 'Использование более совершенного и отказоустойчивого оборудования');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ложное срабатывание системы пожаротушения (и как результат порча собственности)', 'План аварийного восстановления данных');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ложное срабатывание системы пожаротушения (и как результат порча собственности)', 'Использование резервных серверов');


insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (преднамеренное)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (случайное)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с переносных носителей информации (USB-флешки, диски и т.п.)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим вне контролируемой зоны, осуществляемое внешними нарушителями путем анализа трафика, проходящего по канал связи (сюда же можно отнести считывание информации с оптических каналов связи интернет провайдеров)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим внутри контролируемой зоны, осуществляемое внутренними нарушителями путем анализа трафика, проходящего по каналам связи (сюда же можно отнести считывание информации с оптических каналов связи, проложенных внутри здания)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Программные сбои и отказы', 'Тестирование систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Аппаратные сбои и отказы', 'Тестирование систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ошибки проектирования и разработки технических средств', 'Тестирование систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Перебои с электроэнергией', 'Тестирование систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Отказ системы кондиционирования в серверных', 'Тестирование систем');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ложное срабатывание системы пожаротушения (и как результат порча собственности)', 'Тестирование систем');

insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (кража)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератаки со стороны 3-их лиц (порча – удаление или изменение)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кибератака со стороны 3-их лиц (отказ в обслуживание)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Программные сбои и отказы', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Аппаратные сбои и отказы', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (преднамеренное)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Разглашение информации сотрудником отдела (случайное)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и кража информации', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц к компьютеру или серверу и порча (удаление или изменение) информации', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Ошибки проектирования и разработки технических средств', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Кража активов сотрудником организации', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с компьютеров сотрудников по каналам связи (web, email, чаты, ftp, облачные сервисы и т.п.)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Утечка конфиденциальной информации с переносных носителей информации (USB-флешки, диски и т.п.)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим вне контролируемой зоны, осуществляемое внешними нарушителями путем анализа трафика, проходящего по канал связи (сюда же можно отнести считывание информации с оптических каналов связи интернет провайдеров)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение конфиденциальности данных, передаваемых по линиям связи, проходящим внутри контролируемой зоны, осуществляемое внутренними нарушителями путем анализа трафика, проходящего по каналам связи (сюда же можно отнести считывание информации с оптических каналов связи, проложенных внутри здания)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через подключаемые периферийные устройства (USB флешки, телефоны, фотоаппараты и т.п.)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Заражение компьютеров и серверов различными вредоносными программами через Интернет (спам письма, фишинговые сайты, взломанное ПО и т.п.)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Социальная инженерия, с целью выявления данных учетных записей (как результат получение доступа к компьютеру)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных из-за ошибок пользователей, администраторов (неумышленное искажение или удаление файлов с важной информацией или программ)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Нарушение целостности данных (умышленное искажение или удаление файлов с важной информацией или программ работниками)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Неумышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Умышленные действия со стороны сотрудников, приводящие к частичному или полному отказу системы (порча оборудования)', 'Аудит');
insert into danger_countermeasure (danger_name, countermeasure_name) values ('Несанкционированный доступ 3-их лиц и умышленные действия, приводящие к частичному или полному отказу системы (порча оборудования)', 'Аудит');
