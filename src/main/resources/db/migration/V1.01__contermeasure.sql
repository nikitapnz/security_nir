create table countermeasure (
    name varchar(255) not null constraint countermeasure_pkey primary key,
    efficiency double default 0,
    price int default 0,
    source text,
    comment text
);

insert into countermeasure(name, efficiency, price, source, comment) values('Анти-DDoS','9','450000','https://store.softline.ru/kaspersky/kaspersky-ddos-prevention/','Additional Sensor Option на 1 год');
insert into countermeasure(name, efficiency, price, source, comment) values('Системы обнаружения вторжений','10','370000','https://www.securitycode.ru/calc/#open/calc/calc-continent-ids','Стандартный+ключ активации после добавления в корзину на год');
insert into countermeasure(name, efficiency, price, source, comment) values('Бэкап серверов','7','72000','https://www.veeam.com/ru/backup-solution-pricing.html','Veeam Backup Essentials на год');
insert into countermeasure(name, efficiency, price, source, comment) values('Резервное копирование данных на облаке','7','270000','https://mcs.mail.ru/pricing/','Cloud backup за 6 ТБ и одну резервную копию на год');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование DLP систем','10','600000','https://acribia.ru/solutions/infowatch_traffic_monitor','Версия стандарт на год');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование firewall','9','670000','"https://www.vtkt.ru/catalog/security/ipfirewalls/asa_5500_x/cisco_asa_5525_ssd120_k8_mezhsetevoy_ekran_/', null);
insert into countermeasure(name, efficiency, price, source, comment) values('Контроль за неосторожными действиями пользователей','8','600000','https://www.mipko.ru/store','Mipko Employee Monitor за 500 лицензий на год');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование более совершенного и отказоустойчивого оборудования','7',1100000,' -',' -'); --todo сделать что-то
insert into countermeasure(name, efficiency, price, source, comment) values('Выстраивание корпоративной культуры','1','50000',' -','около 100 часов работы персонала, который этим будет заниматься, пускай зп за час 500 рублей');
insert into countermeasure(name, efficiency, price, source, comment) values('Организация бесплатных тренингов и курсов повышения квалификации','1','12000','https://mbschool.ru/seminars/13051','1 тренинг для 10 человек стоит 120000, пускай было 10 тренингов для самых важных сотрудников');
insert into countermeasure(name, efficiency, price, source, comment) values('Меры поощрения за отличную работу','3','1200000',' -','если даже платить 5к в месяц из 500 сотрудников всего 20');
insert into countermeasure(name, efficiency, price, source, comment) values('Повышение компьютерной грамотности персонала','4','6400',' -','4 тренинга за год, тренинг проводит один человек, тренинг длится два часа, зп за час 800 рублей');
insert into countermeasure(name, efficiency, price, source, comment) values('Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)','4','2000000','https://www.osobka.ru/services/tseni-na-uslugi/','всего 500 сотрудников, сбор досье на человека 4000');
insert into countermeasure(name, efficiency, price, source, comment) values('Физическая охрана (службы безопасности или охранники)','5','3000000','https://pravo-detectiv.ru/stoimost/','Служба безопасности на аутсорсе, крупные организации, 250к месяц');
insert into countermeasure(name, efficiency, price, source, comment) values('Видеонаблюдение','5','1560000','https://carcam.ru/product/carcam-videokit-2m-7.html','один комплект стоит 20к за 8 камер, нам нужно 54 камеры + работа персонала (3 человека) зп каждого 40к');
insert into countermeasure(name, efficiency, price, source, comment) values('Пропускная система на базе турникетов и контроллеров (СКУД)','7','1225000','https://securityrussia.com/resheniya/propusknaya-sistema-na-predpriyatii/37330','цена за 7 комплектов');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование охранной сигнализации','4','46500','https://www.csat.ru/business/signaling/','оборудование 10 500 + 500 р ежемесячное тех обслуживание+2500 ежемесячный платеж');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование VPN','7','1200000','https://cloudvpn.pro/ru/','200 рублей в месяц за одно ПК, у нас их 500');
insert into countermeasure(name, efficiency, price, source, comment) values('План аварийного восстановления данных','8','40000',' - ','потребуется 40 часов работы сотрудника, зп которого 1000 рублей в час');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование антивирусных программ','6','1156000','https://www.kaspersky.ru/small-to-medium-business-security/endpoint-select','Расширенный на 500 узлов');
insert into countermeasure(name, efficiency, price, source, comment) values('Системы фильтрации электронной почты корпоративных почтовых ящиков','7','350000','https://spamassassin.apache.org/downloads.cgi?update=202001292230','Имеет в открытом бесплатном доступе, нужно только оплатить работу сотрудника, который это будет настраивать. Зп за час 700 рублей, нужно настроить 500 машин, на одну нужен час');
insert into countermeasure(name, efficiency, price, source, comment) values('Проведение тренингов и бесед по основным приемам социальной инженерии','5','6400',' -','4 тренинга за год, тренинг проводит один человек, тренинг длится два часа, зп за час 800 рублей');
insert into countermeasure(name, efficiency, price, source, comment) values('Обеспечение защиты информации с помощью шифрования данных','9','1500000','http://cybersafesoft.com/product.php?id=1','Enterprise на 500 машин');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование особых процедур подтверждения для всех, кто запрашивает и имеет доступ к конфиденциальной информации','8','288000',' - ','Работа персонала, которая занимается распространением секретных кодов и их проверкой, зп 600 рублей в час, в день 2 часа');
insert into countermeasure(name, efficiency, price, source, comment) values('Определение перечня сотрудников, имеющих доступ к конфиденциальной информации','10','700000',' - ','Трудочасы сотрудника, зп 700 рублей в час, всего 500 сотрудников, на каждого нужно 2 часа относительно решения, имеет он доступ или нет + оформление документов');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование бесперебойного электроснабжения / резервного питания','8','1675000','https://realsolar.ru/ibp/rezerv-shneider-electric/ibp-shneider-electric-20-4-kvt-3f/', null);
insert into countermeasure(name, efficiency, price, source, comment) values('Использование резервных серверов','7','940000','http://dellexpert.ru/server-dlya-kompanii.html','5 серверов');
insert into countermeasure(name, efficiency, price, source, comment) values('Использование резервных кондиционеров','5','390000','https://www.clima-vent.com/mitsubishi-electric-ms-gf35vamu-gf35va-tolko-holod/','5 штук + монтаж');
insert into countermeasure(name, efficiency, price, source, comment) values('Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)','3','108000',' - ','Трудочасы сотрудника, зп 900 рублей в час, потребуется 120 часов');
insert into countermeasure(name, efficiency, price, source, comment) values('Тестирование систем','8','324000',null,'трудочасы сотрудника, зп 900 рублей в час, потребуется 360 часов');
insert into countermeasure(name, efficiency, price, source, comment) values('Аудит','5','450000','https://pravovest-audit.ru/price/','Комплексный обязательный аудит, 1 раз в год');
insert into countermeasure(name, efficiency, price, source, comment) values('Риск менеджмент','6','600000','https://risk-academy.ru/%D1%80%D0%B8%D1%81%D0%BA-%D0%BC%D0%B5%D0%BD%D0%B5%D0%B4%D0%B6%D0%BC%D0%B5%D0%BD%D1%82-1/', null);
