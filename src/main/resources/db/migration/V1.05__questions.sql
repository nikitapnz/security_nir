create table question (
    name varchar(1024) not null constraint question_pkey primary key,
    vulnerability varchar(1024) not null,
    block int not null default 0,
    countermeasure_name varchar(255) not null
        constraint fk8rq7q9imy44qt0jp456n1kfgpc
            references countermeasure
);

create table choice (
    id int IDENTITY not null constraint choice_pkey primary key,
    name varchar(1024)  not null,
    point double not null default 0,
    question_name varchar(1024) not null
        constraint fk8rq7q934344qt0jp456n1kfgpc
            references question
);

create table answer (
    id int IDENTITY not null constraint answer_pkey primary key,
    choice_name varchar(1024) not null
        constraint fk8rq7q934344qt023456n1kfgpc
            references choice,
    asset_name varchar(255) not null
        constraint fk8rq7q9dsfsd4344qt0jp456n1kfgpc
            references asset
);

---- BLOCK 1
insert into question (name, vulnerability, countermeasure_name, block) values ('Решения в области ИБ принимаются компетентными людьми?', 'Отсутствие уполномоченных в области ИБ лиц', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Решения в области ИБ принимаются компетентными людьми?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Решения в области ИБ принимаются компетентными людьми?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Решения в области ИБ принимаются компетентными людьми?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Решения в области ИБ принимаются компетентными людьми?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Дается ли рядовым сотрудникам и подрядчикам ограниченный доступ для выполнения их работы (наименее необходимые и достаточные привилегии)?', 'Отсутствие разграниченного доступа к конфиденциальной информации', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Дается ли рядовым сотрудникам и подрядчикам ограниченный доступ для выполнения их работы (наименее необходимые и достаточные привилегии)?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Дается ли рядовым сотрудникам и подрядчикам ограниченный доступ для выполнения их работы (наименее необходимые и достаточные привилегии)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Дается ли рядовым сотрудникам и подрядчикам ограниченный доступ для выполнения их работы (наименее необходимые и достаточные привилегии)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Дается ли рядовым сотрудникам и подрядчикам ограниченный доступ для выполнения их работы (наименее необходимые и достаточные привилегии)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Обучены ли Ваши сотрудники и подрядчики методам обеспечения безопасности (социальная инженерия, фишинг и прочее)?', 'Необученный основами ИБ персонал', 'Проведение тренингов и бесед по основным приемам социальной инженерии', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Обучены ли Ваши сотрудники и подрядчики методам обеспечения безопасности (социальная инженерия, фишинг и прочее)?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Обучены ли Ваши сотрудники и подрядчики методам обеспечения безопасности (социальная инженерия, фишинг и прочее)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Обучены ли Ваши сотрудники и подрядчики методам обеспечения безопасности (социальная инженерия, фишинг и прочее)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Обучены ли Ваши сотрудники и подрядчики методам обеспечения безопасности (социальная инженерия, фишинг и прочее)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Обращаете ли Вы внимание на изменения в рабочем поведении сотрудников?', 'Невнимательность по отношению к сотрудникам', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Обращаете ли Вы внимание на изменения в рабочем поведении сотрудников?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Обращаете ли Вы внимание на изменения в рабочем поведении сотрудников?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Обращаете ли Вы внимание на изменения в рабочем поведении сотрудников?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Обращаете ли Вы внимание на изменения в рабочем поведении сотрудников?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Проводите ли Вы ежегодную оценку знаний сотрудников в их профессиональной деятельности?', 'Отсутствие четкого понимая уровня квалификации сотрудников', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Проводите ли Вы ежегодную оценку знаний сотрудников в их профессиональной деятельности?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Проводите ли Вы ежегодную оценку знаний сотрудников в их профессиональной деятельности?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Проводите ли Вы ежегодную оценку знаний сотрудников в их профессиональной деятельности?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Проводите ли Вы ежегодную оценку знаний сотрудников в их профессиональной деятельности?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Проводите ли Вы проверку биографических данных потенциальных сотрудников?', 'Отсутствие предварительной проверки рабочих', 'Мероприятия по работе с персоналом (наведение справок, контроль за поведением и т.п)', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Проводите ли Вы проверку биографических данных потенциальных сотрудников?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Проводите ли Вы проверку биографических данных потенциальных сотрудников?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Проводите ли Вы проверку биографических данных потенциальных сотрудников?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Проводите ли Вы проверку биографических данных потенциальных сотрудников?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Существует ли регулярно обновляемый список всех сотрудников с административными привилегиями на ПК / в некоторой бизнес-системе?', 'Устаревший список пользователей, имеющих доступ к конфиденциальной информации', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Существует ли регулярно обновляемый список всех сотрудников с административными привилегиями на ПК / в некоторой бизнес-системе?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Существует ли регулярно обновляемый список всех сотрудников с административными привилегиями на ПК / в некоторой бизнес-системе?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Существует ли регулярно обновляемый список всех сотрудников с административными привилегиями на ПК / в некоторой бизнес-системе?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Существует ли регулярно обновляемый список всех сотрудников с административными привилегиями на ПК / в некоторой бизнес-системе?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Блокируют сотрудники ли экран своего компьютера после того, как они уходят от него на некоторое время?', 'Возможность снятия информации с экрана компьютера посторонними лицами', 'Повышение компьютерной грамотности персонала', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Блокируют сотрудники ли экран своего компьютера после того, как они уходят от него на некоторое время?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Блокируют сотрудники ли экран своего компьютера после того, как они уходят от него на некоторое время?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Блокируют сотрудники ли экран своего компьютера после того, как они уходят от него на некоторое время?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Блокируют сотрудники ли экран своего компьютера после того, как они уходят от него на некоторое время?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Возникают ли ситуации, когда происходит сбой в работе программы или аппаратуры по вине сотрудника?', 'Вывод из строя программы или аппаратуры', 'Повышение компьютерной грамотности персонала', 1);
insert into choice (name, point, question_name) values ('Нет', 0.1, 'Возникают ли ситуации, когда происходит сбой в работе программы или аппаратуры по вине сотрудника?');
insert into choice (name, point, question_name) values ('Изредка', 0.35, 'Возникают ли ситуации, когда происходит сбой в работе программы или аппаратуры по вине сотрудника?');
insert into choice (name, point, question_name) values ('Да', 1, 'Возникают ли ситуации, когда происходит сбой в работе программы или аппаратуры по вине сотрудника?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Возникают ли ситуации, когда происходит сбой в работе программы или аппаратуры по вине сотрудника?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Обращаетесь ли Вы за помощью по охране организации к сотрудникам служб безопасности или обычным охранникам?', 'Отсутствие физической охраны', 'Физическая охрана (службы безопасности или охранники)', 1);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Обращаетесь ли Вы за помощью по охране организации к сотрудникам служб безопасности или обычным охранникам?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Обращаетесь ли Вы за помощью по охране организации к сотрудникам служб безопасности или обычным охранникам?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Обращаетесь ли Вы за помощью по охране организации к сотрудникам служб безопасности или обычным охранникам?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Обращаетесь ли Вы за помощью по охране организации к сотрудникам служб безопасности или обычным охранникам?');

--block 2
insert into question (name, vulnerability, countermeasure_name, block) values ('Есть ли у Вас политика паролей для всех устройств, причем пароли необходимо регулярно менять (раз в четыре месяца и чаще)?', 'Очень слабая или полностью отсутствующая парольная политика', 'Разработка и внедрение политики информационной безопасности (в т.ч. парольной политики)', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Есть ли у Вас политика паролей для всех устройств, причем пароли необходимо регулярно менять (раз в четыре месяца и чаще)?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Есть ли у Вас политика паролей для всех устройств, причем пароли необходимо регулярно менять (раз в четыре месяца и чаще)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Есть ли у Вас политика паролей для всех устройств, причем пароли необходимо регулярно менять (раз в четыре месяца и чаще)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Есть ли у Вас политика паролей для всех устройств, причем пароли необходимо регулярно менять (раз в четыре месяца и чаще)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Есть ли у Вас система управления учетными записями?', 'Отсутствие системы управления учетными записями', 'Контроль за неосторожными действиями пользователей', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Есть ли у Вас система управления учетными записями?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Есть ли у Вас система управления учетными записями?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Есть ли у Вас система управления учетными записями?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Есть ли у Вас система управления учетными записями?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Есть ли у Вас антивирусная защита?', 'Отсутствие антивируса', 'Использование антивирусных программ', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Есть ли у Вас антивирусная защита?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Есть ли у Вас антивирусная защита?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Есть ли у Вас антивирусная защита?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Есть ли у Вас антивирусная защита?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Может ли Ваша организация противодействовать атаке типа "отказ в обслуживании"?', 'Невозможность противодействовать атакам типа "отказ в обслуживании"', 'Анти-DDoS', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Может ли Ваша организация противодействовать атаке типа "отказ в обслуживании"?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Может ли Ваша организация противодействовать атаке типа "отказ в обслуживании"?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Может ли Ваша организация противодействовать атаке типа "отказ в обслуживании"?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Может ли Ваша организация противодействовать атаке типа "отказ в обслуживании"?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Был ли разработан план реагирования на чрезвычайные ситуации в области кибербезопасности?', 'Отсутствие плана реагирования на ЧС', 'План аварийного восстановления данных', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Был ли разработан план реагирования на чрезвычайные ситуации в области кибербезопасности?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Был ли разработан план реагирования на чрезвычайные ситуации в области кибербезопасности?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Был ли разработан план реагирования на чрезвычайные ситуации в области кибербезопасности?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Был ли разработан план реагирования на чрезвычайные ситуации в области кибербезопасности?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Был ли план реагирования недавно пересмотрен и обновлен (последние полгода)?', 'Устаревший план реагирования на ЧС', 'План аварийного восстановления данных', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Был ли план реагирования недавно пересмотрен и обновлен (последние полгода)?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Был ли план реагирования недавно пересмотрен и обновлен (последние полгода)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Был ли план реагирования недавно пересмотрен и обновлен (последние полгода)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Был ли план реагирования недавно пересмотрен и обновлен (последние полгода)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используется ли шифрование для передачи и хранения данных?', 'Отсутствие шифрования данных', 'Обеспечение защиты информации с помощью шифрования данных', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используется ли шифрование для передачи и хранения данных?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Используется ли шифрование для передачи и хранения данных?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используется ли шифрование для передачи и хранения данных?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используется ли шифрование для передачи и хранения данных?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Использует ли веб-служба брандмауэры для защиты от несанкционированного доступа к портам сервера и адресам подсети?', 'Отсутствие брендмауэра', 'Использование firewall', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Использует ли веб-служба брандмауэры для защиты от несанкционированного доступа к портам сервера и адресам подсети?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Использует ли веб-служба брандмауэры для защиты от несанкционированного доступа к портам сервера и адресам подсети?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Использует ли веб-служба брандмауэры для защиты от несанкционированного доступа к портам сервера и адресам подсети?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Использует ли веб-служба брандмауэры для защиты от несанкционированного доступа к портам сервера и адресам подсети?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используются ли какие-либо системы фильтрации корпоративной почты?', 'Незащищенная корпоративная почта', 'Системы фильтрации электронной почты корпоративных почтовых ящиков', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используются ли какие-либо системы фильтрации корпоративной почты?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Используются ли какие-либо системы фильтрации корпоративной почты?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используются ли какие-либо системы фильтрации корпоративной почты?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используются ли какие-либо системы фильтрации корпоративной почты?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Пользуетесь ли Вы такой технологией как VPN?', 'Отсутствие безопасности имеющихся данных', 'Использование VPN', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Пользуетесь ли Вы такой технологией как VPN?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Пользуетесь ли Вы такой технологией как VPN?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Пользуетесь ли Вы такой технологией как VPN?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Пользуетесь ли Вы такой технологией как VPN?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используются ли в организации какие-либо системы предотвращений утечек данных?', 'Возможность утечки данных по каналам связи', 'Использование DLP систем', 2);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используются ли в организации какие-либо системы предотвращений утечек данных?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Используются ли в организации какие-либо системы предотвращений утечек данных?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используются ли в организации какие-либо системы предотвращений утечек данных?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используются ли в организации какие-либо системы предотвращений утечек данных?');

--BLOCK 3
insert into question (name, vulnerability, countermeasure_name, block) values ('Используется ли источник бесперебойного питания для непрерывного контроля?', 'Возможность выхода из строя системы в любое время', 'Использование бесперебойного электроснабжения / резервного питания', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используется ли источник бесперебойного питания для непрерывного контроля?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Используется ли источник бесперебойного питания для непрерывного контроля?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используется ли источник бесперебойного питания для непрерывного контроля?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используется ли источник бесперебойного питания для непрерывного контроля?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Способна ли Ваша организация в короткие сроки устранить проблему отказа кондиционеров в серверных?', 'Перегрев оборудования', 'Использование резервных кондиционеров', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Способна ли Ваша организация в короткие сроки устранить проблему отказа кондиционеров в серверных?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Способна ли Ваша организация в короткие сроки устранить проблему отказа кондиционеров в серверных?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Способна ли Ваша организация в короткие сроки устранить проблему отказа кондиционеров в серверных?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Способна ли Ваша организация в короткие сроки устранить проблему отказа кондиционеров в серверных?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Есть ли резервные сервера, которые могут резко потребоваться в случаях ЧС?', 'Прерывание ключевых бизнес-процессов организации', 'Использование резервных серверов', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Есть ли резервные сервера, которые могут резко потребоваться в случаях ЧС?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Есть ли резервные сервера, которые могут резко потребоваться в случаях ЧС?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Есть ли резервные сервера, которые могут резко потребоваться в случаях ЧС?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Есть ли резервные сервера, которые могут резко потребоваться в случаях ЧС?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используется ли в Вашей организации система видеонаблюдения?', 'Отсутствие мер визуального контроля за ситуацией в организации', 'Видеонаблюдение', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используется ли в Вашей организации система видеонаблюдения?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Используется ли в Вашей организации система видеонаблюдения?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используется ли в Вашей организации система видеонаблюдения?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используется ли в Вашей организации система видеонаблюдения?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Применяется ли охранная сигнализация как дополнительная мера обеспечения безопасности критически важных объектов?', 'Отсуствие предупреждающего о возможной физической угрозе автоматизированного комплекса', 'Использование охранной сигнализации', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Применяется ли охранная сигнализация как дополнительная мера обеспечения безопасности критически важных объектов?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Применяется ли охранная сигнализация как дополнительная мера обеспечения безопасности критически важных объектов?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Применяется ли охранная сигнализация как дополнительная мера обеспечения безопасности критически важных объектов?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Применяется ли охранная сигнализация как дополнительная мера обеспечения безопасности критически важных объектов?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используются ли турникеты и контроллеры для организации пропускной защиты?', 'Отсутствие системы управления доступом на заданную территорию', 'Пропускная система на базе турникетов и контроллеров (СКУД)', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используются ли турникеты и контроллеры для организации пропускной защиты?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Используются ли турникеты и контроллеры для организации пропускной защиты?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используются ли турникеты и контроллеры для организации пропускной защиты?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используются ли турникеты и контроллеры для организации пропускной защиты?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Применяются ли системы обнаружений вторжений?', 'Невозможность распознавать вредоносную активность внутри сети', 'Системы обнаружения вторжений', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Применяются ли системы обнаружений вторжений?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Применяются ли системы обнаружений вторжений?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Применяются ли системы обнаружений вторжений?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Применяются ли системы обнаружений вторжений?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Используете ли Вы по вашему мнение совершенное и отказоустойчивое оборудование (сервера / компьютеры)?', 'Устаревшее и уязвимое к отказам оборудование', 'Использование более совершенного и отказоустойчивого оборудования', 3);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Используете ли Вы по вашему мнение совершенное и отказоустойчивое оборудование (сервера / компьютеры)?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Используете ли Вы по вашему мнение совершенное и отказоустойчивое оборудование (сервера / компьютеры)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Используете ли Вы по вашему мнение совершенное и отказоустойчивое оборудование (сервера / компьютеры)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Используете ли Вы по вашему мнение совершенное и отказоустойчивое оборудование (сервера / компьютеры)?');

--BLOCK 4
insert into question (name, vulnerability, countermeasure_name, block) values ('Вы проводите аудит регулярно? (не менее раза в год)', 'Игнорирование такого мероприятия как аудит', 'Аудит', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Вы проводите аудит регулярно? (не менее раза в год)');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Вы проводите аудит регулярно? (не менее раза в год)');
insert into choice (name, point, question_name) values ('Нет', 1, 'Вы проводите аудит регулярно? (не менее раза в год)');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Вы проводите аудит регулярно? (не менее раза в год)');

insert into question (name, vulnerability, countermeasure_name, block) values ('Вы регулярно выполняете резервное копирование? (не менее одного раза в день)', 'Отсутствие резервного копирования', 'Бэкап серверов', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Вы регулярно выполняете резервное копирование? (не менее одного раза в день)');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Вы регулярно выполняете резервное копирование? (не менее одного раза в день)');
insert into choice (name, point, question_name) values ('Нет', 1, 'Вы регулярно выполняете резервное копирование? (не менее одного раза в день)');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Вы регулярно выполняете резервное копирование? (не менее одного раза в день)');

insert into question (name, vulnerability, countermeasure_name, block) values ('Обслуживаются ли системы пожаротушения по мере необходимости?', 'Отсутствие регулярных проверок систем пожаротужения', 'Тестирование систем', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Обслуживаются ли системы пожаротушения по мере необходимости?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Обслуживаются ли системы пожаротушения по мере необходимости?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Обслуживаются ли системы пожаротушения по мере необходимости?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Обслуживаются ли системы пожаротушения по мере необходимости?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Включено ли автоматическое сканирование съемных носителей, подключаемых к ПК?', 'Незащищенное подключение съемных носителей', 'Использование антивирусных программ', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Включено ли автоматическое сканирование съемных носителей, подключаемых к ПК?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Включено ли автоматическое сканирование съемных носителей, подключаемых к ПК?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Включено ли автоматическое сканирование съемных носителей, подключаемых к ПК?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Включено ли автоматическое сканирование съемных носителей, подключаемых к ПК?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Существует ли в Вашей организации резервное копирование данных на облако?', 'Возможность утери или искажения информации', 'Резервное копирование данных на облаке', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Существует ли в Вашей организации резервное копирование данных на облако?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Существует ли в Вашей организации резервное копирование данных на облако?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Существует ли в Вашей организации резервное копирование данных на облако?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Существует ли в Вашей организации резервное копирование данных на облако?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Проводите ли Вы тестирование всех систем безопасности (перед введением в эксплуатацию/в процессе функционирования)?', 'Возможность внедрения некачественной системы безопасности', 'Тестирование систем', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Проводите ли Вы тестирование всех систем безопасности (перед введением в эксплуатацию/в процессе функционирования)?');
insert into choice (name, point, question_name) values ('Частично', 0.35, 'Проводите ли Вы тестирование всех систем безопасности (перед введением в эксплуатацию/в процессе функционирования)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Проводите ли Вы тестирование всех систем безопасности (перед введением в эксплуатацию/в процессе функционирования)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Проводите ли Вы тестирование всех систем безопасности (перед введением в эксплуатацию/в процессе функционирования)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Проводите ли Вы оценку рисков на регулярной основе (не менее раза в год)?', 'Не выявленные и потенциально опасные риски', 'Риск менеджмент', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Проводите ли Вы оценку рисков на регулярной основе (не менее раза в год)?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Проводите ли Вы оценку рисков на регулярной основе (не менее раза в год)?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Проводите ли Вы оценку рисков на регулярной основе (не менее раза в год)?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Проводите ли Вы оценку рисков на регулярной основе (не менее раза в год)?');

insert into question (name, vulnerability, countermeasure_name, block) values ('Существуют ли ограничения на то, кто может/не может устанавливать программное обеспечение и обновления?', 'Отсутствие разграниченного доступа к конфиденциальной информации', 'Определение перечня сотрудников, имеющих доступ к конфиденциальной информации', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'Существуют ли ограничения на то, кто может/не может устанавливать программное обеспечение и обновления?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'Существуют ли ограничения на то, кто может/не может устанавливать программное обеспечение и обновления?');
insert into choice (name, point, question_name) values ('Нет', 1, 'Существуют ли ограничения на то, кто может/не может устанавливать программное обеспечение и обновления?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'Существуют ли ограничения на то, кто может/не может устанавливать программное обеспечение и обновления?');

insert into question (name, vulnerability, countermeasure_name, block) values ('В момент допуска персонала к конфиденциальной информации, используется ли какая-то дополнительная мера подтверждения личности?', 'Отсутствие особых процедур подтверждения личности в момент доступа к конфиденциальной информации', 'Использование особых процедур подтверждения для всех, кто запрашивает и имеет доступ к конфиденциальной информации', 4);
insert into choice (name, point, question_name) values ('Да', 0.1, 'В момент допуска персонала к конфиденциальной информации, используется ли какая-то дополнительная мера подтверждения личности?');
insert into choice (name, point, question_name) values ('Работаем над этим', 0.35, 'В момент допуска персонала к конфиденциальной информации, используется ли какая-то дополнительная мера подтверждения личности?');
insert into choice (name, point, question_name) values ('Нет', 1, 'В момент допуска персонала к конфиденциальной информации, используется ли какая-то дополнительная мера подтверждения личности?');
insert into choice (name, point, question_name) values ('Не знаю', 0.7, 'В момент допуска персонала к конфиденциальной информации, используется ли какая-то дополнительная мера подтверждения личности?');
