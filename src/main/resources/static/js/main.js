new Vue({
    el: '#app',
    data() {
        return {
            info: null,
            type: 'list',
            selectedAssetName: null,
            selectedAsset: null,
            arrayReportAssets: [],
            reportFinal: []
        };
    },
    updated: function () {
        this.$nextTick(function () {
            if (this.type === 'report') {
                this.reportFinal.filter(e => !e.filled).forEach(rf => {
                    this.fillRoi(rf.index, rf.roi);
                    rf.filled = true;
                });
            }
        })
    },
    mounted() {
        this.getAssets();
    },
    methods: {
        questionSelectedFalse: function(question, choice) {
          question.choices.filter(elem => elem.name !== choice.name).forEach(elem => {
              elem.selected = false;
          })
        },
        chooseAloneReportAsset: function (item) {
            this.info.data.forEach(e => e.selected = false);
            item.selected = true;
            this.updateArrayReportAssets();
        },
        updateArrayReportAssets: function () {
            this.arrayReportAssets = this.info.data.filter(i => i.selected);
        },
        showReports: function () {
            let result = [];
            this.type = 'report';
            this.arrayReportAssets.forEach((a, index) => {
                let obj = {
                    index: index,
                    name: a.name
                };

                this.getRoi(a.name, index, obj)
                    .then(roi => {
                        obj.roi = roi;
                        this.getLossByAsset(a.name, obj)
                            .then(loss => {
                                obj.loss = loss;
                                this.getRisks(a.name, obj)
                                    .then(risks => {
                                        obj.risks = risks;
                                        result.push(obj);
                                        this.reportFinal = result;
                                    })
                            })
                    })
            });
        },
        showReport: function (assetName) {
            this.type = 'report';
            this.arrayReportAssets = this.info.data.filter(i => i.name === assetName);
        },
        getAssets: function () {
            axios
                .get('assets')
                .then(response => {
                    this.info = response;
                });
        },
        getAsset: function (assetName) {
            axios
                .post('assets/asset', { name: assetName  })
                .then(response => (this.selectedAsset = response));
        },
        saveAsset: function () {
            axios
                .put('assets', this.selectedAsset.data)
                .then(response => {
                    this.selectedAsset = null;
                    this.selectedAssetName = null;
                    this.type = 'list';
                    this.getAssets();
                });
        },
        getRisks: function (assetName, report) {
            return axios
                .post('fuzzy/risk', {name: assetName})
                // .then(response => {
                //     report.risks = response;
                // });
        },
        getRoi: function (assetName, index, report) {
            return axios
                .post('countermeasures/roi', {name: assetName})
                // .then(response => {
                //     report.roi = response;
                // });
        },
        fillRoi: function(index, roi) {
            let columnsInfo = roi.data.map(function callback(currentValue) {
                return [currentValue.countermeasureName, currentValue.diff];
            });

            var chart = anychart.column();
            chart.animation(true);

            chart.title('Окупаемость контрмер');
            var series = chart.column(columnsInfo);
            series.tooltip().titleFormat('{%X}');

            series.tooltip()
                .position('center-top')
                .anchor('center-bottom')
                .offsetX(0)
                .offsetY(5)
                .format('₽{%Value}{groupsSeparator: }');
            chart.yScale().minimum(0);
            chart.yAxis().labels().format('₽{%Value}{groupsSeparator: }');
            chart.tooltip().positionMode('point');
            chart.interactivity().hoverMode('by-x');
            chart.xAxis().title('Контрмера');
            chart.yAxis().title('ROI');
            chart.container('roi-chart' + index);
            chart.draw();
            return true;
        },
        getLossByAsset: function (assetName, report) {
            return axios
                .post('countermeasures/loss', {name: assetName})
                // .then(response => {
                //     report.loss = response;
                // });
        }
    },
    computed: {
        loadingReport() {
            for (let index = 0; index < this.reportFinal.length; index++) {
                let elem = this.reportFinal[index];
                if (elem || elem.roi || elem.risks || elem.loss) {
                    return false;
                }
            }
            return true;
        },
        isComplete () {
            if (this.selectedAsset.data.price < 0 || this.selectedAsset.data.price > 2147483647) {
                return false;
            }

            if (!this.selectedAsset.data.active) {
                return true;
            }

            if (this.selectedAsset.data.price === 0) {
                return false;
            }

            if (!this.selectedAsset.data.questions || this.selectedAsset.data.questions.length === 0) {
                return false;
            }
            for (let qIndex = 0; qIndex < this.selectedAsset.data.questions.length; qIndex++) {
                let selected = false;
                for (let chIndex = 0; chIndex < this.selectedAsset.data.questions[qIndex].choices.length; chIndex++) {
                    if (this.selectedAsset.data.questions[qIndex].choices[chIndex].selected) {
                        selected = this.selectedAsset.data.questions[qIndex].choices[chIndex].selected;
                    }
                }
                if (!selected) {
                    return false;
                }
            }

            for (let d = 0; d < this.selectedAsset.data.dangers.length; d++) {
                if (this.selectedAsset.data.dangers[d].chance < 0
                    || this.selectedAsset.data.dangers[d].chance > 1) {
                    return false;
                }
            }
            return true;
        }
    }
});
